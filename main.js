const rocket = document.getElementsByClassName('rocket')[0]

let mousePos = { x: 0, y: 0 }
let rocketPos = { x: 0, y: 0 }

document.body.addEventListener('mousemove', e => {
  const { clientX, clientY } = e
  mousePos = { x: clientX, y: clientY }
})

const rocketVelocity = 0.1

setInterval(() => {
  // calc angle between mouse and rocket
  const theta = Math.atan2(
    mousePos.x - rocketPos.x,
    -(mousePos.y - rocketPos.y),
  )

  // rotate rocket
  rocket.style.transform = `rotate(${theta}rad)`

  // move the rocket
  rocketPos.x += (mousePos.x - rocketPos.x) * rocketVelocity
  rocketPos.y += (mousePos.y - rocketPos.y) * rocketVelocity

  rocket.style.top = `${rocketPos.y}px`
  rocket.style.left = `${rocketPos.x}px`
}, 50)
